# Project: thoitiet.io  
### Simple weather apps that uses data from accio **__Accuweather__**.   
Implemented with **Vue.js**, **NodeJs/Express** and **Bootstrap 4**.  
Made by: Anh Quan Nguyen ~ [GitLab](https://gitlab.com/nguyenquannnn) ~ [LinkedIn](https://www.linkedin.com/in/anh-quan-nguyen-389a1b164/)