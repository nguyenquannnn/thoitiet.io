const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const fetch = require('node-fetch')

require('dotenv').config();

const app = express();

app.use(cors());
app.use(morgan('tiny'));

app.get('/location/geoposition/:lang', (req, res) => {
  const url = "https://dataservice.accuweather.com/locations/v1/cities/geoposition/search";
  if(req.query.long && req.query.lad) {
    const loc = encodeURIComponent(req.query.lad + "," + req.query.long);
    fetch(`${url}?apikey=${process.env.ACCIO_API_KEY}&q=${loc}&language=${req.params.lang}&toplevel=true`)
    .then((response) => response.json())
    .then((json) => {
      res.json(json);
    });
  } else {
    next();
  }
});

app.get('/condition/forecasts/accio_id/:lang/:id', (req, res) => {
  const url = "https://dataservice.accuweather.com/forecasts/v1/daily/5day/";
  fetch(`${url}/${req.params.id}?apikey=${process.env.ACCIO_API_KEY}&language=${req.params.lang}&details=true&metric=true`)
  .then((response) => response.json())
  .then((json) => res.json(json));
});

app.get('/condition/current/accio_id/:lang/:id', (req, res) => {
  const url = "https://dataservice.accuweather.com/currentconditions/v1";
  fetch(`${url}/${req.params.id}?apikey=${process.env.ACCIO_API_KEY}&language=${req.params.lang}&details=true`)
  .then((response) => response.json())
  .then((json) => res.json(json));
});

app.get('/city/:name', (req, res) => {
  const url = "https://dataservice.accuweather.com/locations/v1/cities/search";
  fetch(`${url}?q=${req.params.name}&apikey=${process.env.ACCIO_API_KEY}&language=en&details=true`)
  .then((response) => response.json())
  .then((json) => res.json(json));
});

function errorHandler(error, req, res, next) {
  res.status(res.statusCode || 500);
  res.json({
    message: error.message,
  });
}

function notFoundHandler(req, res, next) {
  res.status(404);
  const error = new Error('Not Found');
  next(error);
}

app.use(errorHandler);
app.use(notFoundHandler);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('listening on port:' + port);
});
