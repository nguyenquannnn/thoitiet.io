"use strict";
const API_URL = "https://thoitietio-mgbthwswhp.now.sh"
var tt = {
  init: function() {
    Vue.use(Vuex);
    //VUEX STORES
    const vxStore = new Vuex.Store({
      state: {
        settings: {
          language: "Tieng Viet",
          langCode: "vi",
          autoDetect: false,
          celcius: true,
          forecast: 5,
        },
        isSearching: false,
        home: {
          location: {
            longitude: undefined,
            latitude: undefined,
            city: undefined,
            country: undefined,
            accio_id: undefined,
          },
          condition: {
            temp: undefined,
            tempMax: undefined,
            tempMin: undefined,
            tempWindChill: undefined,
            windSpeed: undefined,
            windDirection: undefined,
            status: undefined,
            icon: undefined,
          },
          choices: undefined,
          forecastCons: [],
          forecastWarning: undefined,
          time: undefined,
        },
        search: {
          location: {
            longitude: undefined,
            latitude: undefined,
            city: undefined,
            country: undefined,
            accio_id: undefined,
          },
          condition: {
            temp: undefined,
            tempMax: undefined,
            tempMin: undefined,
            tempWindChill: undefined,
            windSpeed: undefined,
            windDirection: undefined,
            status: undefined,
            icon: undefined,
          },
          choices: undefined,
          forecastCons: [],
          forecastWarning: undefined,
          time: undefined,
        }
      },
      mutations: {
        updateGeolocation: function(state, coords) {
          if (typeof coords === 'object' && !state.isSearching) {
            state.home.location.longitude = coords.longitude;
            state.home.location.latitude = coords.latitude;
          } else if (typeof coords === 'object' && state.isSearching) {
            state.search.location.longitude = coords.longitude;
            state.search.location.latitude = coords.latitude;
          }
        },
        updateLocation: function(state, location) {
          if (typeof location === 'object' && !state.isSearching) {
            state.home.location.city = location.city;
            state.home.location.country = location.country;
            state.home.location.accio_id = location.accio_id;
          } else if (typeof location === 'object' && state.isSearching) {
            state.search.location.city = location.city;
            state.search.location.country = location.country;
            state.search.location.accio_id = location.accio_id;
          }
        },
        updateConditionAll: function(state, condition) {
          if (condition && !state.isSearching) {
            state.home.condition = condition;
          } else if (condition && state.isSearching) {
            state.search.condition = condition;
          }
        },
        updateForecasts: function(state, conditions) {
          if (!state.isSearching) {
            state.home.forecastCons = conditions;
            state.home.forecastWarning = conditions.forecastWarning;
          } else {
            state.search.forecastCons = conditions;
            state.home.forecastWarning = conditions.forecastWarning;
          }
        },
        newHomeChoices: function(state, choices) {
          state.home.choices = choices;
        },
        newSearchChoices: function(state, choices) {
          if (state.isSearching) {
            state.search.choices = choices;
          } else {
            state.home.choices = choices;
          }
        },
        updateSearching: function(state, searching) {
          state.isSearching = searching;
        },
      },
    });
    //COMPONENTS
    const bus = new Vue();
    const searchComponent = {
      props: {
        autodetect: {
          type: Boolean,
          default: false,
        },
        searching: {
          type: Boolean,
          default: false,
        }
      },
      // <button v-on:click="settings.autoDetect = !settings.autoDetect">Search</button>
      template: `<form class="mx-5 my-2" v-if="isDisplay" v-on:submit.prevent="searchHandler">
              <div class="row">
                <div class="col-md-11">
                <input  class="form-control mr-2 mr- mr-sm-2" v-model="city" type="search" placeholder="Hanoi">
                </div>
                <div class="col-md-1">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Tìm Kiếm</button>
                </div>
              </div>
            </form>`,
      data: function() {
        return {
          city: undefined,
        }
      },
      computed: {
        isDisplay: function() {
          return !this.autodetect;
        },
        isSearched: function() {
          return this.$root.$route.path === '/search';
        }
      },
      methods: {
        searchHandler: function() {
          if (this.city) {
            bus.$emit('search', {
              name: this.city,
              searching: this.searching,
            });
          } else {
            bus.$emit('error', 'Tên địa điểm không hợp lệ!');
          }
        },
      }
    }

    const navComponent = {
      template: `<nav class="navbar navbar-expand-lg navbar-light bg-light">
       <router-link to="/" class="navbar-brand" href="#">Thoitiet</router-link>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation" style="">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor03">
          <ul class="navbar-nav mr-auto" v-on:click="clickHandler">
            <li class="nav-item" v-bind:class="{active: isHome}">
              <router-link to="/home" class="nav-link" href="#">Home <span class="sr-only">(current)</span></router-link>
            </li>
            <li class="nav-item" v-bind:class="{active: isSearch}">
              <router-link to="/search" class="nav-link" href="#">Search</router-link>
            </li>
            <li class="nav-item" v-bind:class="{active: isAbout}">
              <router-link to="/about" class="nav-link" href="#">About</router-link>
            </li>
          </ul>
        </div>
        </nav>`,
      // <search v-if="!isSearch"></search>
      components: {
        'search': searchComponent,
      },
      computed: {
        isSearch: function() {
          return this.$route.path === '/search';
        },
        isHome: function() {
          return this.$route.path === '/home';
        },
        isAbout: function() {
          return this.$route.path === '/about';
        }
      },
      methods: {
        clickHandler: function(e) {
          if (e.target.tagName === "li") {
            e.target.classList.add("active");
          }
        },
      }
    }
    const locationComponent = {
      template: `<div>
      <h1> {{ location.city }}, {{ location.country }}</h1>
      </div>`,
      props: {
        location: {
          type: Object,
        },
      }
    }

    const multipleComponent = {
      template: `<div class="card bg-light mb-3"">
        <div class="card-header">Lựa chọn thành phố:</div>
  <ul class="list-group list-group-flush">
    <li v-on:click="clickHandler" class="list-group-item list-group-item list-group-item-action"" v-for="(c, index) in choices" :data-index="index">
     {{c.LocalizedName + ", " + c.Country.LocalizedName}}</li>
  </ul>
</div>`,
      props: {
        choices: {
          type: Array,
          required: true,
        },
        searching: {
          type: Boolean,
          default: false,
        }
      },
      methods: {
        clickHandler: function(e) {
          bus.$emit("select", {
            choice: this.choices[e.target.dataset.index],
            searching: this.searching,
          });
        }
      }
    }

    const timeComponent = {
      props: {
        condition: {
          type: Object,
        },
      },
      computed: {
        imageSource: function() {
          return "./assets/icon/" + this.condition.icon + ".png";
        },
      },
      template: `<div>
      <div class="row">
        <h6>{{ condition.title }}</h6>
        </div>
        <div class="row">
          <div class="col-md-8">
          <div>{{ condition.description }} </div>
          <div>Tỉ lệ mưa: {{ condition.rainChance }}% </div>
          </div>
          <div class="col-md-4"
          <img v-if="condition.icon" :src="imageSource">
          </div>
        </div>
      </div>
      `
    }
    const fcComponent = {
      props: {
        conditions: {
          type: Array,
        },
        metric: {
          type: Boolean,
          default: true,
        }
      },
      components: {
        'day': timeComponent,
        'night': timeComponent,
      },
      template: `<div class="card-group">
      <div class="card" v-for="fc in conditions" >
  <div class="card-body">
    <h5 class="card-title">{{ fc.date }}</h5>
    <h6 class="card-subtitle mb-2 text-muted">{{ fc.min }} {{ unitDegree }}
    đến {{ fc.max}} {{ unitDegree}}</h6>
    <section class="card-text">
    <day :condition="fc.day"></day>
    <night :condition="fc.night"></night>
    </section>
  </div>
    </div>
</div>`,
      computed: {
        unitDegree: function() {
          return this.metric ? "°C" : "°F";
        },
        unitSpeed: function() {
          return this.metric ? "km/h" : "miles/h";
        },
      },
      watch: {
        metric: function() {
          if (!this.metric) {
            this.cond.temp = (this.cond.temp * 9 / 5) + 32;
            this.cond.tempMax = (this.cond.tempMax * 9 / 5) + 32;
            this.cond.tempMin = (this.cond.tempMin * 9 / 5) + 32;
            this.cond.tempWindChill = (this.cond.tempWindChill * 9 / 5) + 32;
            this.cond.windSpeed *= 0.621371;
          }
        },
      }
    };

    const ccComponent = {
      props: {
        cond: {
          type: Object,
          default: undefined
        },
        loc: {
          type: Object,
          default: undefined
        },
        metric: {
          type: Boolean,
          default: true,
        },
        warning: {
          type: String,
        },
      },
      computed: {
        unitDegree: function() {
          return this.metric ? "°C" : "°F";
        },
        unitSpeed: function() {
          return this.metric ? "km/h" : "miles/h";
        },
        iconSource: function() {
          return "./assets/icon/" + this.cond.icon + ".png";
        },
        time: function() {
          if (this.cond.time) {
            return new Date(this.cond.time * 1000).toUTCString();
          }

        }
      },
      watch: {
        metric: function() {
          if (!this.metric) {
            this.cond.temp = (this.cond.temp * 9 / 5) + 32;
            this.cond.tempMax = (this.cond.tempMax * 9 / 5) + 32;
            this.cond.tempMin = (this.cond.tempMin * 9 / 5) + 32;
            this.cond.tempWindChill = (this.cond.tempWindChill * 9 / 5) + 32;
            this.cond.windSpeed *= 0.621371;
          }
        }
      },
      //<a href="#" class="btn btn-primary">Go somewhere</a>
      template: `
          <div class="card" v-if="loc.accio_id">
            <h5 class="card-header"><location :location="loc"></location></h5>
            <div class="card-body">
              <div class="row">
                <div class="col-md-8 col-sm-6">
                  <h5 class="card-title display-3">{{ cond.status }}</h5>
                  <p class="card-text display-4">{{ warning }}</p>
                </div>
                <div class="col-md-2 col-sm-4">
                  <span class="display-3">{{ cond.temp + unitDegree }}</span>
                </div>
                <div class="col-md-2" col-sm-2>
                  <img :src="iconSource" class="align-middle img-fluid">
                </div>
              </div>
              <div class="row">
                <ul class="my-3 mx-1">
                  <li>Nhiệt độ cao nhất: {{ cond.tempMax + unitDegree }}</li>
                  <li>Nhiệt độ thấp nhất: {{ cond.tempMin + unitDegree }}</li>
                  <li>Nhiệt độ phong hàn: {{ cond.tempWindChill + unitDegree}}</li>
                  <li>Gió: {{ cond.windSpeed + " " + unitSpeed }} hướng {{ cond.windDirection }} </li>
                </ul>
              </div>
              <hr class="mx-3">
              <p class="font-italic font-weight-light">Vào thời điểm: {{ time }}</p>
              </div>
            </div>
          </div>`,
      components: {
        'location': locationComponent,
      }
    }

    const infoComponent = {
      props: {
        searching: {
          type: Boolean,
          default: false,
        }
      },
      template: `
      <div>
      <currentcondition v-if="location && condition" :loc="location" :cond="condition" :warning="warning"></currentcondition>
      <forecast :conditions="forecasts"></forecast>
      </div>`,
      computed: {
        condition: function() {
          if (this.searching) {
            return this.$root.$store.state.search.condition;
          } else {
            return this.$root.$store.state.home.condition;
          }
        },
        warning: function() {
          if (this.searching) {
            return this.$root.$store.state.search.forecastWarning;
          } else {
            return this.$root.$store.state.home.forecastWarning;
          }
        },
        forecasts: function() {
          if (this.searching) {
            return this.$root.$store.state.search.forecastCons;
          } else {
            return this.$root.$store.state.home.forecastCons;
          }
        },
        location: function() {
          if (this.searching) {
            return this.$root.$store.state.search.location;
          } else {
            return this.$root.$store.state.home.location;
          }
        }
      },
      components: {
        'forecast': fcComponent,
        'currentcondition': ccComponent,
        'location': locationComponent,
        //'multiple': multipleComponent,
      },
    }

    const errorComponent = {
      props: {
        message: {
          type: String,
          default: undefined,
        }
      },
      template: `<div v-if="message" class="alert alert-danger" role="alert">
      {{ message }}
      </div>`,
    }

    const homeComponent = {
      template: `
      <div>
        <error :message="this.$root.error"></error>
        <search v-if="this.$root.error"></search>
        <info></info>
        <choices v-if="choices" :choices="choices"></choices>
        </div>
      `,
      components: {
        'info': infoComponent,
        'choices': multipleComponent,
        'error': errorComponent,
        'search': searchComponent,
      },
      computed: {
        choices: function() {
          return this.$root.$store.state.home.choices;
        },
      },
    }

    const discoverComponent = {
      template: `<div>
      <error :message="this.$root.error"></error>
      <div class="my-2 pt-15">
      <search :searching="true"></search>
      </div>
      <choices v-if="choices" :choices="choices"></choices>
      <info :searching="true" v-if="searches"></info>
      </div>`,
      components: {
        'search': searchComponent,
        'info': infoComponent,
        'choices': multipleComponent,
        'error': errorComponent,
      },
      computed: {
        choices: function() {
          return this.$root.$store.state.search.choices;
        },
        searches: function() {
          return this.$root.$store.state.search.location.accio_id;
        }
      },
    }
    const aboutComponent = {
      template: `<div class="mx-auto text-center">
      <img src="https://i.pinimg.com/236x/9f/6b/4c/9f6b4c592add51582bd8abf3aa1dad29--cat-cafe--bit.jpg" alt="logo" class="logo-rounded img-fluid">
      <p class="display-3">Làm bởi Anh Quan Nguyen</p>
      <p class="my-3 text-uppercase">Vue.js + VueX + Bootstrap + NodeJS + Accuweather</p>
      <a href="https://gitlab.com/nguyenquannnn">GitLab</a>
      <a href="https://www.linkedin.com/in/anh-quan-nguyen-389a1b164/">LinkedIn</a>
      </div>`,
    }

    const routes = [{
        path: "/",
        component: homeComponent,
      },
      {
        path: "/home",
        component: homeComponent,
      },
      {
        path: "/search",
        component: discoverComponent,
      },
      {
        path: "/about",
        component: aboutComponent,
      },
    ];

    const router = new VueRouter({
      routes: routes
    });
    //VUE ROOT
    tt.vm = new Vue({
      el: "#app",
      router: router,
      store: vxStore,
      components: {
        'menubar': navComponent,
      },
      data: {
        errorHome: undefined,
        errorSearching: undefined,
      },
      created: function() {
        bus.$on("search", this.searchHandler);
        bus.$on("select", this.selectHandler);
        bus.$on("error", (err) => this.error = err);
        if (this.checkGeoPos()) {
          //this.settings.autoDetect = true;
          this.getPrecisePosition().then(this.successLocate, this.errorLocate)
            .then(this.get).then(this.updateLocation)
            .then(() => {
              if (this.location.accio_id) {
                // const currentCondURL = "http://dataservice.accuweather.com/currentconditions/v1/" +
                //   this.location.accio_id + "?apikey=" + ACCIO_API_KEY + "&language=vi&details=true";
                const currentCondURL = API_URL + "/condition/current/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                return currentCondURL;
              }
            }).then(this.get).then(this.updateCondition)
            .then(() => {
              if (this.location.accio_id) {
const forecastURL = API_URL + "/condition/forecasts/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                return forecastURL;
              }
            }).then(this.get).then(this.parseForecasts).catch(err => {
              this.error = err.message;
            });
        } else {
          this.reloadHomeLocation();
        }
      },
      computed: {
        location: function() {
          if (this.searching) {
            return this.$store.state.search.location;
          } else {
            return this.$store.state.home.location;
          }
        },
        condition: function() {
          if (this.searching) {
            return this.$store.state.search.condition;
          } else {
            return this.$store.state.home.condition;
          }
        },
        searching: function() {
          return this.$store.state.isSearching;
        },
        languageCode: function() {
          return this.$store.state.settings.langCode;
        },
        error: {
          get() {
            return this.searching ? this.errorSearching : this.errorHome;
          },
          set(value) {
            if (this.searching) {
              this.errorSearching = value;
            } else {
              this.errorHome = value;
            }
          },
        },
      },
      methods: {
        getPrecisePosition: function() {
          return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
              function(position) {
                resolve(position);
              },
              function(error) {
                reject(error);
              });
          });
        },
        checkGeoPos: function() {
          return ("geolocation" in navigator);
        },
        successLocate: function(position) {
          this.$store.commit('updateGeolocation', {
            longitude: position.coords.longitude,
            latitude: position.coords.latitude
          });
          return API_URL + "/location/geoposition/" + this.languageCode + "?lad=" + this.location.latitude + "&long=" + this.location.longitude;
        },
        errorLocate: function() {
          this.autoDetec = false;
          if (this.isRegistered()) {
            this.reloadHomeLocation();
            var p = new Promise((resolve, reject) => {
              if (this.location.accio_id) {
                const currentCondURL = API_URL + "/condition/current/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                resolve(currentCondURL);
              }
            });
            p.then(this.get).then(this.updateCondition).then(() => {
              if (this.location.accio_id) {
                // const forecastURL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" +
                //   this.location.accio_id + "?apikey=" + ACCIO_API_KEY + "&language=vi&details=true&metric=true";
                const forecastURL = API_URL + "/condition/forecasts/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                return forecastURL;
              }
            }).then(this.get).then(this.parseForecasts);
          } else {
            this.error = 'Khổng thể xác định được vị trí của bạn. Xin hãy search vị trí hiện tại của bạn!'
          }
        },
        updateLocation: function(response) {
          if (response && !Array.isArray(response)) {
            var temp = {};
            temp.city = this.language !== "en" ? response.LocalizedName :
              response.EnglishName;
            temp.country = this.language !== "en" ? response.Country.LocalizedName :
              response.Country.LocalizedName;
            temp.accio_id = response.Key;
            this.$store.commit('updateLocation', temp);
          } else if (response && Array.isArray(response)) {
            return Promise.reject(response);
          }
        },
        updateCondition: function(response) {
          if (response) {
            var condition = {};
            condition.temp = response[0].Temperature.Metric.Value;
            condition.tempMax = response[0].TemperatureSummary.Past24HourRange.Maximum.Metric.Value;
            condition.tempMin = response[0].TemperatureSummary.Past24HourRange.Minimum.Metric.Value;
            condition.status = response[0].WeatherText;
            condition.windDirection = this.language !== "en" ? response[0].Wind.Direction.Localized :
              response[0].Wind.Direction.English;
            condition.windSpeed = response[0].Wind.Speed.Metric.Value;
            condition.tempWindChill = response[0].WindChillTemperature.Metric.Value;
            condition.icon = response[0].WeatherIcon;
            condition.time = response[0].EpochTime;
            this.$store.commit('updateConditionAll', condition);
          }
        },
        parseForecasts: function(response) {
          if (response) {
            this.forecastCons = [];
            response.DailyForecasts.forEach((cond) => {
              var temp = {};
              temp.date = cond.Date.split("T")[0];
              temp.day = {};
              temp.day.title = "Ngày";
              temp.day.description = cond.Day.LongPhrase;
              temp.day.rainChance = cond.Day.RainProbability;
              temp.day.icon = cond.Day.Icon;
              temp.night = {};
              temp.night.title = "Đêm";
              temp.night.description = cond.Night.LongPhrase;
              temp.night.rainChance = cond.Night.RainProbability;
              temp.night.icon = cond.Night.Icon;
              temp.max = cond.Temperature.Maximum.Value;
              temp.min = cond.Temperature.Minimum.Value;
              this.forecastCons.push(temp);
            });
            this.forecastWarning = response.Headline.Text;
            this.forecastCons.forecastWarning = this.forecastWarning;
            this.$store.commit('updateForecasts', this.forecastCons);
          }
        },
        get: function(url) {
          return new Promise((resolve, reject) => {
            tt.ajax = tt.ajax || new XMLHttpRequest();
            if (url) {
              tt.ajax.open("GET", url);
              tt.ajax.addEventListener("load", function() {
                if (tt.ajax.status >= 200 && tt.ajax.status <= 304) {
                  resolve(JSON.parse(tt.ajax.response));
                } else {
                  reject(Error(tt.ajax.statusText));
                }
              });
              tt.ajax.send();
            }
          })
        },
        searchHandler: function(value) {
          if (value.searching) {
            this.$store.commit('updateSearching', true);
          }
          this.$store.commit('newSearchChoices', undefined);
          var url = API_URL + '/city/' + value.name;

          this.get(url).then(this.updateLocation).catch((response) => {
              this.$store.commit('newSearchChoices', response);
              throw new Error("Nhiều thành phố cùng tên");
            }).then(() => {
              if (this.location.accio_id) {
                const currentCondURL = API_URL + "/condition/current/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                return currentCondURL;
              }
            }).then(this.get).then(this.updateCondition)
            .then(() => {
              if (this.location.accio_id) {
const forecastURL = API_URL + "/condition/forecasts/accio_id/" + this.languageCode + "/" + this.location.accio_id;
                return forecastURL;
              }
            }).then(this.get).then(this.parseForecasts).catch(err => {
              this.error = err.message;
            });
        },
        selectHandler: function(response) {
          this.error = undefined;
          if (this.searching) {
            this.$store.state.search.choices = undefined;
          } else {
            this.$store.state.home.choices = undefined;
          }
          this.updateLocation(response.choice);
          var p = new Promise((resolve, reject) => {
            if (this.location.accio_id) {
              const currentCondURL = API_URL + "/condition/current/accio_id/" + this.languageCode + "/" + this.location.accio_id;
              resolve(currentCondURL);
            }
          });
          p.then(this.get).then(this.updateCondition).then(() => {
            if (this.location.accio_id) {
const forecastURL = API_URL + "/condition/forecasts/accio_id/" + this.languageCode + "/" + this.location.accio_id;
              return forecastURL;
            }
          }).then(this.get).then(this.parseForecasts).then(() => {
            this.$store.commit('updateSearching', false);
          }).then(() => {
            this.reloadHomeLocation();
          }).catch(err => {
            this.error = err.message;
          });
        },
        reloadHomeLocation: function() {
          var home = localStorage.getItem("home");
          if (home) {
            home = JSON.parse(home);
            this.$store.commit('updateLocation', home.location);
          } else {
            this.registerHomeLocation();
          }
        },
        registerHomeLocation: function() {
          // if (this.$store.state.home.location && this.$store.state.home.accio_id) {
          localStorage.setItem("home", JSON.stringify(this.$store.state.home));
          // }
        },
        isRegistered: function() {
          return localStorage.getItem("home");
        }
      }
    });
  }
}
window.addEventListener("DOMContentLoaded", tt.init);
